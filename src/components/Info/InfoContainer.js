import React, { Component } from 'react'
import { Redirect } from "react-router-dom"

import NavigationButton from '../helpers/NavigationButton'

class InfoContainer extends Component {
  render() {
    const { match } = this.props
    console.log('props', this.props);
    if (match.params.itemId === 'link-1') return <Redirect to='/weather' />
    if (match.params.itemId === 'link-2') return (
      <div style={{display: 'inlineFlex'}}>
        <div className='navigation'>
          <NavigationButton to='/weather' title='weather' />
        </div>
      </div>
    )
    return (
      <div>
        <h3>{match.params.itemId}</h3>
      </div>
    )
  }
}

export default InfoContainer
