import React, { Component } from 'react'
import { Route, Switch } from "react-router-dom"
import nanoid from 'nanoid'

import NavigationButton from '../helpers/NavigationButton'
import NoMatch from '../NoMatch/NoMatch'
import InfoContainer from './InfoContainer'

const links = {
  info: [
    {to: '/link-1', title: 'link-1'},
    {to: '/link-2', title: 'link-2'},
    {to: '/link-3', title: 'link-3'},
    {to: '/link-4', title: 'link-4'}
  ]
}

class Info extends Component {
  matchLinkPath = () => {
    return (
      links.info.map(el => el.title).join('|')
    )
  }

  render() {
    const { match } = this.props
    return (
      <div className='info'>
        <div className='info_navigation'>
          <nav className='navigation'>
            {links.info.map(({ to, title }) => {
              return (
                <NavigationButton key={nanoid(5)} to={`${match.url}${to}`} title={title} />
              )
            })
            }
          </nav>
        </div>

        <Switch>
          <Route path={`${match.url}/:itemId(${this.matchLinkPath()})`} component={InfoContainer} />
          <Route
            exact
            path={match.url}
            render={() => <h3>Please select a link.</h3>}
          />
          <Route component={NoMatch} />
        </Switch>
      </div>
    )
  }
}

export default Info
