import React, { Component } from 'react'
import WeatherBlock from './WeatherBlock'
import { compose } from 'redux'
import { DragSource, DropTarget } from 'react-dnd'

import { ItemTypes } from '../../constants'

const blockSource = {
	beginDrag(props) {
		return {
			draggedBlock: props.blockDD
		}
	}
}

const blockTarget = {
	hover(props, monitor) {
		const { draggedBlock } = monitor.getItem()
		const { blockDD: overBlock } = props
		if (draggedBlock !== overBlock) {
			props.moveBlock({draggedBlock, overBlock})
		}
	}
}

class WeatherBlockContainer extends Component {
  render() {
    const { connectDragSource, connectDropTarget, ...props } = this.props
    return connectDragSource(connectDropTarget(
      <div>
        <WeatherBlock {...props} />
      </div>
    ))
  }
}

export default compose(
  DragSource(ItemTypes.WEATHER_BLOCK_TYPE, blockSource, (connect, monitor) => ({
  	connectDragSource: connect.dragSource(),
  	isDragging: monitor.isDragging()
  })),
  DropTarget(ItemTypes.WEATHER_BLOCK_TYPE, blockTarget, connect => ({
  	connectDropTarget: connect.dropTarget(),
  }))
)(WeatherBlockContainer)
