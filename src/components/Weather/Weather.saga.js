import {takeEvery, call, put, fork} from 'redux-saga/effects'

import * as actions from './Weather.actionTypes'
import * as api from '../core/API/api'

function* initLocationWeather() {
  yield takeEvery(actions.INIT_LOCATION_WEATHER, init)
}

function* addOrInitWeather() {
  yield takeEvery(actions.ADD_OR_INIT_WEATHER, add)
}

function* init(action) {
  const {lat, lng} = action
  const cityName = yield call(api.getData, {lat, lng, key: 'city'})
  const {results} = cityName
  const city = results[0].address_components[0].long_name
  const data = yield call(api.getData, {lat, lng, key: 'weather'})
  const { list: [{id, main: {temp}, weather: [{description, icon}]}] } = data
  const tempToSet = (temp > 0) ? `+${Math.round(temp)}°` : `${Math.round(temp)}°`
  const weather = {name: city, id, block: 'local', temp: tempToSet, description, icon}
  yield put({type: actions.LOCATION_WEATHER_WAS_INIT, weather})
}

function* add(action) {
  const {addressFix: city, block} = action
  const coord = yield call(api.getData, {city, key: "coord"})
  const {results} = coord
  if (!results) return
  const {geometry: {location: {lat, lng}}} = results[0]
  const data = yield call(api.getData, {lat, lng, key: 'weather'})
  const { list: [{id, main: {temp}, weather: [{description, icon}]}] } = data
  const tempToSet = (temp > 0) ? `+${Math.round(temp)}°` : `${Math.round(temp)}°`
  const weather = {name: city, id, block, temp: tempToSet, description, icon}
  yield put({type: actions.WEATHER_WAS_INIT_OR_ADD, weather})
}

export default function* watch() {
  yield [
    fork(initLocationWeather),
    fork(addOrInitWeather)
  ];
}
