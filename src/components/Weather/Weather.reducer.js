import { cloneDeep } from 'lodash'

import * as actions from './Weather.actionTypes'

const defaultState = {
  cityWeather: [],
  repeat: null,
  workingLocal: false,
  working: [],
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case actions.INIT_LOCATION_WEATHER:
      return {
        ...state,
        workingLocal: true,
      }

    case actions.ADD_OR_INIT_WEATHER:
      return {
        ...state,
        working: [...state.working, action.id]
      }

    case actions.LOCATION_WEATHER_WAS_INIT:
    case actions.WEATHER_WAS_INIT_OR_ADD: {
      const index = state.cityWeather.findIndex(({ id }) => action.weather.id === id)
      const i = state.working.indexOf(action.weather.id)
      let workingClone = state.working.slice()
      workingClone.splice(i, 1)
      let clone = cloneDeep(state.cityWeather)
      clone.push(action.weather)
      return {
        ...state,
        repeat: index > -1 ? action.weather.id : null,
        cityWeather: index > -1 ? state.cityWeather : clone,
        workingLocal: false,
        working: workingClone,
      }
    }

    case actions.DELETE_WEATHER: {
      let clone = cloneDeep(state.cityWeather)
      const index = state.cityWeather.findIndex(({ id }) =>  id === action.id)
      clone.splice(index, 1)
      return {
        ...state,
        cityWeather: clone,
      }
    }

    case actions.DRAG_DROP_ITEM:
     return {
       ...state,
       cityWeather: action.dragDropCityWeather
     }

    case actions.RESET_REPEAT:
      return ({
        ...state,
        repeat: null,
      })

    default: return state
  }
}
