import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose, bindActionCreators } from 'redux'
import update from 'immutability-helper'
import { DragDropContext } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'
import OutsideClick from 'react-onclickout'
import classNames from 'classnames'
import nanoid from 'nanoid'
import { isEqual, cloneDeep } from 'lodash'

import Button from '../helpers/Button'
import WeatherBlockContainer from './WeatherBlockContainer'
import Modal from '../modal/Modal'
import CitySelectPopup from '../popups/CitySelectPopup'
import '../../stylesheets/app.scss'
import * as AC from './Weather.AC'

class Weather extends Component {
  static propTypes = {
    cityWeather: PropTypes.array,
    repeat: PropTypes.number,
    workingLocal: PropTypes.bool,
    working: PropTypes.array,
  }

  state = {
    showModal: false,
    closeId: {},
		cardsById: {},
		cardsByIndex: [],
		blocks: [],
		inTable: false,
    inBlock: {},
    activeBlock: null,
    timerId: null,
    view: {},
  }

  componentDidMount() {
    const { AC: { initLocationWeather } } = this.props
    const viewToSet = window.localStorage.getItem('view')
    if (viewToSet) this.setState({ view: JSON.parse(viewToSet) })

    if (navigator.geolocation && !window.localStorage.blocks) {
      this.setState({ blocks: [{block: 'local', title: null}] })
			navigator.geolocation.getCurrentPosition(function(position) {
        const location = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        }
        initLocationWeather(location)
        return
      })
    }

    if (window.localStorage.blocks) {
      this.setState({ blocks: JSON.parse(window.localStorage.blocks) })
      if (!window.localStorage.local || this.props.cityWeather.length) return
      const cities = JSON.parse(window.localStorage.local)
      this.initWeather(cities)
    }
  }

  componentDidUpdate(nextProps, nextState) {
    const { view, blocks } = this.state
    const { cityWeather } = this.props
    if (!isEqual(view, nextState.view)) {
      window.localStorage.setItem('view', JSON.stringify(view))
    }
    if (!isEqual(blocks, nextState.blocks) || !isEqual(cityWeather, nextProps.cityWeather)) {
      this.setAppStorage()
    }
  }

	setAppStorage = () => {
  	const { blocks } = this.state
    const { cityWeather } = this.props
    if (!cityWeather.length) window.localStorage.removeItem('local')
    if (!blocks.length) window.localStorage.removeItem('blocks')
		if (cityWeather.length) window.localStorage.setItem('local', JSON.stringify(cityWeather))
		if (blocks.length) window.localStorage.setItem('blocks', JSON.stringify(blocks))
	}

  initWeather = (cities) => {
    const { AC: { addOrInitWeather } } = this.props
    const citiesToSet = cities.map(({ name, block, id }) => ({ name, block, id }))
    const index = citiesToSet.length-1
    for (var i = 0; i <= index; i++) {
      addOrInitWeather(citiesToSet[i].name, citiesToSet[i].block, citiesToSet[i].id)
    }
  }

  saveBlockTitle = (blockId, title) => {
    const { blocks } = this.state
    const index = blocks.map(({ block }) => block).indexOf(blockId)
    delete blocks[index].title
    blocks[index].title = title
    this.setState({ blocks })
    window.localStorage.setItem('blocks', JSON.stringify(blocks))
  }

  showModal = (block) => () => {
    this.setState({ showModal: block })
  }

  hideModal = () => {
    this.setState({ showModal: false })
  }

  toggle = (key, id) => () => {
    if (id && key === 'block') {
      this.setState({ closeId: {} })
      this.timerId = setTimeout(() => this.setState({ closeId: { [key]: id } }), 1000)
      return
    }
    this.stopTimer()
    this.setState({ closeId: { [key]: id } })
  }

  timerId = null

  stopTimer = () => {
    clearTimeout(this.timerId)
    this.timerId = null
  }

  toggleView = (block) => () => {
    this.setState(({ view }) => ({ view: {...view, [block]: !view[block]} }))
  }

  deleteItem = (id) => () => {
    this.props.AC.deleteWeather(id)
  }

  onClickOut = () => {
    this.setState({ showModal: false })
  }

	clickInTable = () => {
		this.setState({ inTable: true })
	}

	clickOutTable = () => {
		this.setState({ inTable: false })
	}

  clickInBlock = (block) => () => {
    this.setState(({ inBlock }) => (
      {
        inBlock: {...inBlock, [block]: !inBlock[block]},
        activeBlock: block
      }
    ))
  }

  clickOutBlock = (block, activeBlock) => () => {
    if (block === activeBlock) {
      this.setState({ inBlock: {} })
    }
  }

  moveCard = ({ draggedId, overId, overIndex, overBlock }) => {
    const { cityWeather } = this.props
		const { card, index } = this.findCard(draggedId)
		let overIndexToSet
		if (overIndex) {
			overIndexToSet = overIndex
		} else {
			if (overId) {
				overIndexToSet = cityWeather.findIndex(el => el.id === overId)
			}
			if (!overId) {
				overIndexToSet = 0
			}
			delete card.block
			card.block = overBlock
		}

    const dragDropCityWeather = update(cityWeather, {
			$splice: [[index, 1], [overIndexToSet, 0, card]],
			},
		)
    this.props.AC.dragDropItem(dragDropCityWeather)
    this.setAppStorage()
	}

  findCard = (id) => {
    const { cityWeather } = this.props
		const card = cityWeather.find(c => c.id === id)

		return {
			card,
			index: cityWeather.indexOf(card),
		}
	}

	moveBlock = ({ draggedBlock, overBlock }) => {
		const { blocks } = this.state
		const draggedIndex = blocks.indexOf(draggedBlock)
		const overIndex = blocks.indexOf(overBlock)
		const newBlocks = update(blocks, {
			$splice: [[draggedIndex, 1], [overIndex, 0, draggedBlock]],
			},
		)
		this.setState({ blocks: newBlocks })
	}

  addBlock = () => {
    const block = nanoid(5)
    this.setState(({ blocks }) => ({ blocks: [ ...blocks, { block, title: null } ] }))
  }

  removeBlock = (block) => () => {
    const { cityWeather, AC: { deleteWeather } } = this.props
    const { blocks, view } = this.state
    const index = blocks.findIndex(el => el.block === block)
    const clone = blocks.slice()
    clone.splice(index, 1)
    let viewClone = cloneDeep(view)
    if (viewClone[block]) delete viewClone[block]
    this.setState({ blocks: clone, view: viewClone })
    const weatherBlock = cityWeather.filter(el => el.block === block)
    const weatherBlockLength = weatherBlock.length
    for (let i = 0; i < weatherBlockLength; i++) {
      deleteWeather(weatherBlock[i].id)
    }
  }

  renderBlock = (block) => {
    const {
      cityWeather,
      AC: { resetRepeat },
      repeat,
      workingLocal,
      working,
    } = this.props
    const {
      closeId,
      inBlock,
      blocks,
      activeBlock,
      view,
    } = this.state

		const weatherBlock = cityWeather.filter(el => el.block === block.block)
    const titleToSet = blocks.find(el => el.block === block.block).title || 'Название блока'

    return (
      <WeatherBlockContainer
        toggleDeleteButton={this.toggle}
        deleteItem={this.deleteItem}
        moveCard={this.moveCard}
        findCard={this.findCard}
        resetRepeat={resetRepeat}
        repeat={repeat}
        closeId={closeId}
				weatherBlock={weatherBlock}
				onClick={this.showModal(block)}
				block={block.block}
        blockDD={block}
				moveBlock={this.moveBlock}
        clickInBlock={this.clickInBlock}
        clickOutBlock={this.clickOutBlock}
        inBlock={inBlock}
        workingLocal={workingLocal}
        working={working}
        removeBlock={this.removeBlock}
        title={titleToSet}
        saveBlockTitle={this.saveBlockTitle}
        activeBlock={activeBlock}
        toggleView={this.toggleView}
        view={view}
      />
    )
  }

	renderBlocks = () => {
		const { showModal, blocks } = this.state
    const classNameButtonAddBlock = classNames('button', 'button_add-block')
		return (
			<OutsideClick onClickOut={this.clickOutTable}>
        <div onClick={this.clickInTable}>
					{blocks.map(block => (
						<div key={`block-${block.block}`}>
	            {this.renderBlock(block)}
		          {showModal === block && (
		            <Modal>
		              <CitySelectPopup
		                onClickOut={this.onClickOut}
		                hideModal={this.hideModal}
										block={block.block}
		              />
		            </Modal>
		          )}
		        </div>
					))}
          <Button
            name='add-plus'
            className={classNameButtonAddBlock}
            onClick={this.addBlock}
          />
				</div>
			</OutsideClick>
		)
	}

  render() {
		const  { inTable } = this.state
		const active = classNames(
			'weather_container',
			{'weather_container-active': inTable}
		)
    return (
      <div className={active}>
				{this.renderBlocks()}
      </div>
    )
  }
}

const mapStateToProps = ({ weatherReducer }) => {
  return {
    ...weatherReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
    AC: bindActionCreators(AC, dispatch)
  }
}

export default compose(
  DragDropContext(HTML5Backend),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Weather)
