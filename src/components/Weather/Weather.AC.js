import * as actions from './Weather.actionTypes'

export function initLocationWeather({ lat, lng }) {
  return {
    type: actions.INIT_LOCATION_WEATHER,
    lat,
    lng,
  }
}

export function addOrInitWeather(addressFix, block, id) {
  return {
    type: actions.ADD_OR_INIT_WEATHER,
    addressFix,
    block,
    id
  }
}

export function deleteWeather(id) {
  return {
    type: actions.DELETE_WEATHER,
    id
  }
}

export function resetRepeat() {
  return {
    type: actions.RESET_REPEAT
  }
}

export function dragDropItem(dragDropCityWeather) {
  return {
    type: actions.DRAG_DROP_ITEM,
    dragDropCityWeather
  }
}
