import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { compose } from 'redux'
import { DragSource, DropTarget } from 'react-dnd'
import Loading from 'react-loading-animation'

import Button from '../helpers/Button'
import { ItemTypes } from '../../constants'
import '../../stylesheets/app.scss'

const cardSource = {
	beginDrag(props) {
		return {
			draggedId: props.id,
			draggedBlock: props.block
		}
	}
}

const cardTarget = {
	hover(props, monitor) {
		const { draggedId, draggedBlock } = monitor.getItem()
		const { id: overId, block: overBlock } = props
		let overIndex
		if (draggedId !== overId && draggedBlock === overBlock) {
			const { index } = props.findCard(overId)
			overIndex = index
		} else {
			overIndex = null
		}
		if (draggedId !== overId) {
			props.moveCard({ draggedId, overId, overIndex, overBlock })
		}
	}
}

class WeatherItem extends Component {
	static propTypes = {
		name: PropTypes.string,
		temp: PropTypes.string,
		id: PropTypes.number,
		icon: PropTypes.string,
    key:  PropTypes.oneOfType([
			PropTypes.number,
			PropTypes.string
		]),
		repeat: PropTypes.number,
		closeId: PropTypes.number,
		working: PropTypes.array,
		block: PropTypes.string.isRequired,
		deleteItem: PropTypes.func,
		isDragging: PropTypes.bool,
		connectDragSource: PropTypes.func,
		connectDropTarget: PropTypes.func,
		toggleDeleteButton: PropTypes.func,
  }

	state = {
		highlight: false,
		backlight: false
	}

	componentWillReceiveProps(nextProps) {
		const { id, repeat, resetRepeat } = this.props
		if (nextProps.repeat && nextProps.repeat === id && nextProps.repeat !== repeat ) {
			this.setState({ highlight: true })
			setTimeout(() => this.setState({ backlight: true }), 1000)
			setTimeout(() => this.setState({ highlight: false, backlight: false }), 2000)
			resetRepeat()
		}
	}

  render() {
    const {
      name,
      temp,
      id,
      icon,
      closeId,
      deleteItem,
      isDragging,
      connectDragSource,
      connectDropTarget,
			toggleDeleteButton,
			working,
			block,
			view,
    } = this.props

		const { highlight, backlight } = this.state

		if (working.includes(id)) return <Loading key='loader'/>

		const itemClassname = classNames(
			'weather_item',
			{'weather_item-row': !!view[block]},
			{'highlight' : highlight},
			{'backlight' : backlight}
		)

		const buttonClassname = classNames(
			'button_close',
			{'button_close_active': closeId === id}
		)

    const opacity = isDragging ? 0 : 1

    return connectDragSource(
			connectDropTarget(
        <div
					style={{opacity}}
					className={itemClassname}
					onMouseEnter={toggleDeleteButton('item', id)}
					onMouseLeave={toggleDeleteButton('block', block)}
				>
          <div title={name}>{name}</div>
          <div>{temp}</div>
          <img src={`http://openweathermap.org/img/w/${icon}.png`} alt='' />
					<Button
						name='close'
						className={buttonClassname}
						onClick={deleteItem(id)}
					/>
        </div>
  		)
    )
  }
}

export default compose(
  DragSource(ItemTypes.WEATHER_ITEM_TYPE, cardSource, (connect, monitor) => ({
  	connectDragSource: connect.dragSource(),
  	isDragging: monitor.isDragging()
  })),
  DropTarget(ItemTypes.WEATHER_ITEM_TYPE, cardTarget, connect => ({
  	connectDropTarget: connect.dropTarget(),
  }))
)(WeatherItem)
