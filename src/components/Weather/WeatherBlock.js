import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'redux'
import { DropTarget } from 'react-dnd'
import OutsideClick from 'react-onclickout'
import classNames from 'classnames'
import Loading from 'react-loading-animation'

import { ItemTypes } from '../../constants'
import WeatherItem from './WeatherItem'
import Button from '../helpers/Button'
import Input from '../helpers/Input'

const cardTarget = {
  hover(props, monitor) {
    const { draggedId } = monitor.getItem()
    const { block, weatherBlock } = props
    if (weatherBlock && !weatherBlock.length) {
      props.moveCard({draggedId, overId: null, overIndex: null, overBlock: block})
    }
  }
}

class WeatherBlock extends Component {
  static propTypes = {
    block: PropTypes.string.isRequired,
    activeBlock: PropTypes.string,
    inBlock: PropTypes.object,
    title: PropTypes.string,
    weatherBlock: PropTypes.array.isRequired,
    closeId: PropTypes.object,
    workingLocal: PropTypes.bool,
    toggleDeleteButton: PropTypes.func,
    saveBlockTitle: PropTypes.func,
    clickInBlock: PropTypes.func,
    clickOutBlock: PropTypes.func,
    removeBlock: PropTypes.func,
    onClick: PropTypes.func,
    connectDropTarget: PropTypes.func,
  }

  state = {
    pencil: false,
  }

  togglePencil = (pencil) => {
    this.setState({ pencil })
  }

  render() {
    const {
      weatherBlock,
      closeId,
      onClick,
      connectDropTarget,
      clickInBlock,
      clickOutBlock,
      removeBlock,
      block,
      inBlock,
      workingLocal,
      toggleDeleteButton,
      title,
      saveBlockTitle,
      activeBlock,
      toggleView,
      view,
      ...props
    } = this.props

    const { pencil } = this.state

    const activeBlockClassName = classNames(
      'weather_block',
      {'weather_block-active': inBlock[block]}
    )
    const buttonBlockClassName = classNames(
      {'button_block': !view[block]}
    )
    return connectDropTarget (
      <div id='block'>
        <OutsideClick onClickOut={clickOutBlock(block, activeBlock)}>
          <div
            className={activeBlockClassName}
            onClick={clickInBlock(block)}
            onMouseEnter={toggleDeleteButton('block', block)}
  					onMouseLeave={toggleDeleteButton('block', null)}
          >
            <div className='weather_block-title'>
              <Input
                title={title}
                titleDefault='Название блока'
                pencil={pencil}
                togglePencil={this.togglePencil}
                saveBlockTitle={saveBlockTitle}
                block={block}
                activeBlock={activeBlock}
              />
            </div>
            <div className='weather_block-body'>
              {weatherBlock.map(el => (
                <WeatherItem
                  {...props}
                  {...el}
                  key={el.id ? el.id : 'loading-item'}
                  closeId={closeId.item === el.id ? closeId.item : null}
                  toggleDeleteButton={toggleDeleteButton}
                  block={block}
                  view={view}
                />
              ))}
              {block === 'local' && workingLocal && <Loading key='loader'/>}
              {closeId && closeId.block === block && !pencil && (
                <Button
    							name='close'
    							className='button_close button_close_active button_close_block'
                  onClick={removeBlock(block)}
    						/>
              )}
              <div className={buttonBlockClassName}>
                <Button
                  name='add-plus'
                  className='button'
                  onClick={onClick}
                  toggleDeleteButton={toggleDeleteButton}
                  block={block}
                />
                <Button
                  name='toggle'
                  className='button'
                  onClick={toggleView(block)}
                  toggleDeleteButton={toggleDeleteButton}
                  block={block}
                />
              </div>
            </div>
          </div>
        </OutsideClick>
      </div>
    )
  }
}

export default compose(
  DropTarget(ItemTypes.WEATHER_ITEM_TYPE, cardTarget, connect => ({
  	connectDropTarget: connect.dropTarget(),
  }))
)(WeatherBlock)
