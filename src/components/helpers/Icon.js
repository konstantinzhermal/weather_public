import React from "react"
import PropTypes from 'prop-types'

import Icons from "../../static/icons/svg-icons.svg"

const Icon = ({ name }) => (
  <svg>
    <use
      href={`${Icons}#${name}`}
    />
  </svg>
)

Icon.propTypes = {
  name: PropTypes.string.isRequired,
}

export default Icon
