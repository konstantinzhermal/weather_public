import React from 'react'
import { NavLink } from "react-router-dom"
import PropTypes from 'prop-types'
import classNames from 'classnames'

import '../../stylesheets/app.scss'

function NavigationButton(props) {
  const {
    to,
    from,
    title,
    homeNavigation,
  } = props

  const css = classNames('navigation_button', {
    'navigation_button_home': homeNavigation
  })

  return (
    <div className={css}>
      <div>
        <NavLink to={to} from={from} activeClassName='navigation_button_active'>
          {title}
        </NavLink>
      </div>      
    </div>
  )
}

NavigationButton.propTypes = {
  to: PropTypes.string.isRequired,
  title: PropTypes.string,
}

export default NavigationButton
