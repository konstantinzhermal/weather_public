import React, { Component } from 'react'
import { connect } from 'react-redux'
import PlacesAutocomplete from 'react-places-autocomplete'

import '../../stylesheets/app.scss'
import { addOrInitWeather } from '../Weather/Weather.AC'

class CityAutocomplete extends Component {
  state = {
    address: '',
  }

  onChange = (address) => this.setState({ address })

  handleSelect = (address) => {
    const { block } = this.props
    this.setState({ address })
    const addressFix = address.split(', ')[0]
    this.props.addOrInitWeather(addressFix, block)
    this.setState({ address: '' })
  }

  render() {
    const { onChange, handleSelect } = this
    const { address } = this.state
    const inputProps = {
      value: address,
      onChange: onChange,
      type: 'search',
      placeholder: 'Введите название города',
      autoFocus: true,
    }
    const cssClasses = {
    root: 'form-group',
    input: 'form-control',
    autocompleteContainer: 'my-autocomplete-container'
    }

    return (
      <div>
        <PlacesAutocomplete
          inputProps={inputProps}
          classNames={cssClasses}
          onSelect={handleSelect}
        />
      </div>
    )
  }
}

export default connect(null, { addOrInitWeather })(CityAutocomplete)
