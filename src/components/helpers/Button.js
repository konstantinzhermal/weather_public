import React from 'react'
import PropTypes from 'prop-types'

import Icon from './Icon'
import '../../stylesheets/app.scss'

function Button(props) {
  const {
    name,
    onClick,
    className,
    block,
    toggleDeleteButton,
  } = props

  return (
    <div
      onClick={onClick}
      className={className}
      onMouseEnter={toggleDeleteButton && toggleDeleteButton('block', null)}
      onMouseLeave={toggleDeleteButton && toggleDeleteButton('block', block)}
    >
      <Icon name={name} />
    </div>
  )
}

Button.propTypes = {
  name: PropTypes.string,
  block: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  toggleDeleteButton: PropTypes.func,
}

export default Button
