import React, { Component } from 'react'
import OutsideClick from 'react-onclickout'
import PropTypes from 'prop-types'

import Button from './Button.js'

class Input extends Component {
  static propTypes = {
    block: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    titleDefault: PropTypes.string,
    pencil: PropTypes.bool,
    saveBlockTitle: PropTypes.func.isRequired,
    togglePencil: PropTypes.func,
  }

  state = {
    edit: false,
    value: this.props.title,
  }

  edit = () => {
    this.setState({ edit: true })
  }

  onChange = (e) => {
    this.setState({ value: e.target.value })
  }

  saveTitle = () => {
    const {
      saveBlockTitle,
      block
    } = this.props
    const { value } = this.state

    saveBlockTitle(block, value)
    this.setState({ edit: false })
  }

  deleteTitle = () => {
    this.setState({ value: '' })
  }

  renderEdit = () => {
    const { titleDefault } = this.props
    const { value } = this.state
    return (
      <div className='input-edit'>
        <input
          autoFocus
          placeholder='Введите название блока'
          value={value !== titleDefault ? (value || '') : ''}
          onChange={this.onChange}
        />
        <div>
          <div>
            <Button
              name='checked'
              onClick={this.saveTitle}
            />
          </div>
          <div>
            <Button
              name='cancel'
              onClick={this.deleteTitle}
            />
          </div>
        </div>
      </div>
    )
  }

  renderPencil = () => {
    return (
      <div className='button_pencil'>
        <Button
          name='pencil'
          className='button_pencil'
          onClick={this.edit}
        />
      </div>
    )
  }

  onClickOut = (block, activeBlock) => () => {
    if (block === activeBlock) {
      this.setState({ edit: false })
      this.props.togglePencil(false)
    }
  }

  render() {
    const {
      pencil,
      title,
      togglePencil,
      block,
      activeBlock,
    } = this.props
    const { edit } = this.state

    return (
      <div>
        <OutsideClick onClickOut={this.onClickOut(block, activeBlock)}>
          <div>
            {!edit ? (
              <div
                className='input'
                onMouseEnter={() => togglePencil(true)}
                onMouseLeave={() => togglePencil(false)}
              >
                <div>
                  {title}
                </div>
                {pencil && this.renderPencil()}
              </div>
            ) : (
              this.renderEdit()
            )}
          </div>
        </OutsideClick>
      </div>
    )
  }
}

export default Input
