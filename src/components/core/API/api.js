import { TOKEN, TOKEN2 } from '../../../constants'

export function getData(params) {
  const url = getUrl(params)
  return fetchData(url)
}

const fetchData = async (url) => {
  const response = await fetch(url)
  const responseJSON = response.json()
  return responseJSON
}

const getUrl = ({key, lat, lng, city}) => {
  switch (key) {
    case 'city': {
      const url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${TOKEN2}&language=ru&result_type=locality`
      return url
    }
    case 'weather': {
      const url = `https://api.openweathermap.org/data/2.5/find?lat=${lat}&lon=${lng}&units=metric&type=like&APPID=${TOKEN}`
      return url
    }
    case 'coord': {
      const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${city}&key=${TOKEN2}`
      return url
    }
    default:
  }
}
