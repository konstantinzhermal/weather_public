import { combineReducers } from 'redux'
import weatherReducer from '../Weather/Weather.reducer'

export default combineReducers({
    weatherReducer
})
