import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'

import watch from '../Weather/Weather.saga'
import reducer from './reducers'

const sagaMiddleware = createSagaMiddleware()
const store = createStore(reducer, applyMiddleware(sagaMiddleware))
sagaMiddleware.run(watch)

export default store
