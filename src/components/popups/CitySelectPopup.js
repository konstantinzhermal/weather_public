import React from 'react'
import OutsideClick from 'react-onclickout'

import Button from '../helpers/Button'
import CityAutocomplete from '../helpers/CityAutocomplete'

function CitySelectPopup(props) {
  const { onClickOut, hideModal, block } = props
  return (
    <div className="modal">
      <OutsideClick onClickOut={() => onClickOut()}>
        <div id='modal' className="modal__city-select">
          <Button
            name='cancel'
            onClick={hideModal}
            className='modal__city-select-button'
          />
          <div className='autocomplete'>
            <CityAutocomplete block={block} />
          </div>
        </div>
      </OutsideClick>
    </div>
  )
}

export default CitySelectPopup
