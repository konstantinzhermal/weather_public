import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Switch, Redirect, NavLink } from "react-router-dom"
import nanoid from 'nanoid'

import NavigationButton from './components/helpers/NavigationButton'
import Weather from './components/Weather/Weather'
import Home from './components/Home/Home'
import Info from './components/Info/Info'
import NoMatch from './components/NoMatch/NoMatch'
import store from './components/core/store'
import logo from './static/image/umbrella.png'

const links = {
  home: [
    {to: '/info', title: 'Info'},
    {to: '/weather', title: 'Weather'}
  ]
}

class App extends Component {
  render() {
    return (
      <Provider store = {store}>
        <Router>
          <div className='main'>
            <nav className='navigation'>
              <div className='button_home'>
                <NavLink key={nanoid(5)} to='/home'>
                  <img src={logo} />
                </NavLink>
              </div>
              {links.home.map(({ to, title }) => {
                return (
                  <NavigationButton homeNavigation key={nanoid(5)} to={to} title={title} />
                )
              })
              }
            </nav>

            <div>
              <Switch>
                <Route
                  exact
                  path="/"
                  render={() => <Redirect to='/home' />}
                />
                <Route path="/home" component={Home} />
                <Route path="/info" component={Info} />
                <Route path="/weather" component={Weather} />
                <Route component={NoMatch} />
              </Switch>
            </div>
          </div>
        </Router>
      </Provider>
    )
  }
}

export default App
